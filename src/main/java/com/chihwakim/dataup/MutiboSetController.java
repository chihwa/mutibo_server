package com.chihwakim.dataup;

import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import com.chihwakim.dataup.repository.MutiboSet;
import com.chihwakim.dataup.repository.MutiboSetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import retrofit.http.GET;

// Tell Spring that this class is a Controller that should
// handle certain HTTP requests for the DispatcherServlet
@Controller
public class MutiboSetController {
	public static final String SET_SVC_PATH = "/set";
	public static final String DATA_PARAMETER = "data";
	public static final String ID_PARAMETER = "id";


	@Autowired
	MutiboSetRepository mutiboSetRepo;

	//Successful login will see True.
	@RequestMapping(value = MutiboSetSvcApi.MUTIBO_LOGIN_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Boolean getLogin(){
		return Boolean.TRUE;
	}
	
	//Return all MutiboSets from MutiboSet collection
	@RequestMapping(value = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<MutiboSet> getMutiboSetList() {
		return Lists.newArrayList(mutiboSetRepo.findAll());
	}
	
	//Return one MutiboSet for the ID
	@RequestMapping(value = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH + "/{id}", method = RequestMethod.GET)
	public @ResponseBody MutiboSet getVideoById(@PathVariable("id") String id,
			HttpServletResponse response) {
		MutiboSet mutiboSet = null;
		try {
			mutiboSet = mutiboSetRepo.findOne(id);
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		return mutiboSet;
	}
	
	//Add a MutiboSet in the Collection
	@RequestMapping(value = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH, method = RequestMethod.POST)
	public @ResponseBody MutiboSet addMutiboSet(@RequestBody MutiboSet m) {
		MutiboSet saved = mutiboSetRepo.save(m);
		return saved;
	}

	//Increase presented count, and optionally increase obscure count
	//If obscure count is too high, remove the record.
	@RequestMapping(value = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH + "/{id}", method = RequestMethod.POST)
	public @ResponseBody MutiboSet markObscureForId(@PathVariable("id") String id, 
			@RequestParam("obscure") String obscure) {
		MutiboSet loadedMutiboSet = mutiboSetRepo.findOne(id);
		int currentPresentedCnt = loadedMutiboSet.getPresentedCnt();
		loadedMutiboSet.setPresentedCnt(currentPresentedCnt+1);
		
		if(obscure.compareTo("true")==0){
			int currentObscureCnt = loadedMutiboSet.getIssueCnt();
			loadedMutiboSet.setIssueCnt(currentObscureCnt+1);
		}
		
		
		if (loadedMutiboSet.getPresentedCnt()>5 &&
				(((double)loadedMutiboSet.getIssueCnt()/(double)loadedMutiboSet.getPresentedCnt())>0.2D)){
			mutiboSetRepo.delete(loadedMutiboSet);
			return loadedMutiboSet;
		} else{
			MutiboSet saved = mutiboSetRepo.save(loadedMutiboSet);
			return saved;
		}
	}

}