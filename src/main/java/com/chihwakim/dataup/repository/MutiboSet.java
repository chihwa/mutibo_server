package com.chihwakim.dataup.repository;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fluentinterface.ReflectionBuilder;
import com.fluentinterface.builder.Builder;

@Entity
public class MutiboSet {

	public static MutiboSetBuilder create() {
		return ReflectionBuilder.implementationFor(MutiboSetBuilder.class).create();
	}

	public interface MutiboSetBuilder extends Builder<MutiboSet> {
		public MutiboSetBuilder withQuestion(String question);
		public MutiboSetBuilder withChoice(String choice);
		public MutiboSetBuilder withAnswer(int answer);
		public MutiboSetBuilder withExplain(String explain);
		public MutiboSetBuilder withPresentedCnt(int presentedCnt);
		public MutiboSetBuilder withIssueCnt(int issueCnt);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	private String question;
	private String choice;
	private int answer;
	private String explain;
	private int presentedCnt;
	private int issueCnt;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public int getAnswer() {
		return answer;
	}

	public void setAnswer(int answer) {
		this.answer = answer;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public int getPresentedCnt() {
		return presentedCnt;
	}

	public void setPresentedCnt(int presentedCnt) {
		this.presentedCnt = presentedCnt;
	}

	public int getIssueCnt() {
		return issueCnt;
	}

	public void setIssueCnt(int issueCnt) {
		this.issueCnt = issueCnt;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof MutiboSet)
				&& getId() == ((MutiboSet) obj).getId();
	}

}
