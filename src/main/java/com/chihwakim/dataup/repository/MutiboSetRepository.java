package com.chihwakim.dataup.repository;

import com.chihwakim.dataup.MutiboSetSvcApi;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * An interface for a repository that can store Video
 * objects and allow them to be searched by id
 * 
 * @author chihwa
 *
 */
@RepositoryRestResource(collectionResourceRel = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH, path = MutiboSetSvcApi.MUTIBO_SET_SVC_PATH)
public interface MutiboSetRepository extends MongoRepository<MutiboSet, String>{

}
