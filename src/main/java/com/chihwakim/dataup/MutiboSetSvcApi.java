package com.chihwakim.dataup;

import java.util.Collection;

import com.chihwakim.dataup.repository.MutiboSet;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface MutiboSetSvcApi {

	public static final String ID_PARAMETER = "id";

	// The path where we expect the VideoSvc to live
	public static final String MUTIBO_SET_SVC_PATH = "set";

	// The path for login
	public static final String MUTIBO_LOGIN_SVC_PATH = "login";

	//Return True for successful login and false otherwise
	@GET("/login")
	public Boolean getLogin();
	
	//Return all the MutiboSets
	@GET(MUTIBO_SET_SVC_PATH)
	public Collection<MutiboSet> getMutiboSetList();

	//Return one MutiboSet for the ID
	@GET(MUTIBO_SET_SVC_PATH + "/{id}")
	public MutiboSet getMutiboSetById(@Path("id") String id);

	//Add a MutiboSet
	@POST(MUTIBO_SET_SVC_PATH)
	public MutiboSet addMutiboSet(@Body MutiboSet v);
	
	//Mark the MutiboSet for the ID as obscure
	@POST(MUTIBO_SET_SVC_PATH + "/{id}")
	public MutiboSet markObscureForId(@Path("id") String id);

}
