### What is this repository for? ###

* Mutibo Server: Spring MVC quiz server for Android clients
* Version 1.0

### How do I get set up? ###

* Summary of set up
    * Local 
        * Import build.gradle then run on IDE: src\main\java\org\magnum\dataup\application.java
    * AWS 
        * Install MongoDB and Apache Tomcat
        * Copy server.jks to apache-tomcat and apache-tomcat/bin/src/main/resources
        * gradle clean build
        * copy builds/lib/mutibo-server-0.4.war.original to apache-tomcat/webapps/mutibo-server-0.4.war
        * Modify apache-tomcat/config/server.xml
            * Add below in place of default Connector:
            
            ```
            <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
                       maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
                       keystoreFile="/home/ec2-user/apache-tomcat/server.jks" keystorePass="mutibo_server"
                       clientAuth="false" sslProtocol="TLS" />
             ```
             
            * Add below in Host node
            
            ```
            <Context docBase="mutibo-server-0.4" path="" />
            ```

* Database configuration
    * Default local MongoDB installation

* How to run tests
    * Add these Mutibo quiz sets for testing:

```
#!linux

curl -H "Content-Type: application/json" -d '{"question":"Question..","choice":"choice1;choice2;choice3;choice4", "answer":0, "explain":"Explanation..","presentedCnt":0,"issueCnt":0}' http://localhost:8080/set

curl -H "Content-Type: application/json" -d '{"question":"Which of these is not like the others?","choice":"Hackers;War Games;Tron;Snow White", "answer":3, "explain":"Snow White is the only move in this set that is not about computer science.","presentedCnt":0,"issueCnt":0}' http://localhost:8080/set

curl -H "Content-Type: application/json" -d '{"question":"Which movie is released in 2014?","choice":"Iron Man 3;Sherlock Holmes;Interstella;Jurassic World", "answer":2, "explain":"Iron Man 3 - 2013, Sherlock Holmes - 2009, Interstella - 2014, Jurassic World - 2015","presentedCnt":0,"issueCnt":0}' http://localhost:8080/set
```


### Question ? ###

* Chi Hwa Kim (chihwa93@gmail.com)

